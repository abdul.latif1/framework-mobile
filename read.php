<?php
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Headers: X-Requested-With,
     Content-Type, Origin, Cache-Control, Pragma, Authorization, 
     Accept, Accept-Encoding");
    header("Content-Type: application/json;");
    
    include_once 'database.php';
    include_once 'user.php';

    $database = new DB();
    $db = $database->getConnection();

    $items = new User($db);

    $stmt = $items->getUsers();
    $itemCount = $stmt->rowCount();

    if($itemCount > 0){
        
        $userArr = array();
       

        while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            extract($row);
            $e = array(
                "id" => $id,
                "nama_depan" => $nama_depan,
                "nama_belakang" => $nama_belakang,
                "email_id" => $email_id
            );

            array_push($userArr, $e);
        }
        echo json_encode($userArr);
    }

?>
