<?php
    class User{

        // conn
        private $conn;

        // table
        private $dbTable = "user";

        // col
        public $id;
        public $nama_depan;
        public $nama_belakang;
        public $email_id;
      
        // db conn
        public function __construct($db){
            $this->conn = $db;
        }

        // GET Users
        public function getUsers(){
            $sqlQuery = "SELECT id, nama_depan, nama_belakang, email_id
               FROM " . $this->dbTable . "";
            $stmt = $this->conn->prepare($sqlQuery);
            $stmt->execute();
            return $stmt;
        }

        // CREATE User
        public function createUser(){
            $sqlQuery = "INSERT INTO
                        ". $this->dbTable ."
                    SET
                    nama_depan = : nama_depan, 
                    nama_belakang = :nama_belakang, 
                    email_id = :email_id";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
            // sanitize
            $this->nama_depan=htmlspecialchars(strip_tags($this->nama_depan));
            $this->nama_belakang=htmlspecialchars(strip_tags($this->nama_belakang));
            $this->email_id=htmlspecialchars(strip_tags($this->email_id));
                   
            // bind data
            $stmt->bindParam(":nama_depan", $this->nama_depan);
            $stmt->bindParam(":nama_belakang", $this->nama_belakang);
            $stmt->bindParam(":email_id", $this->email_id);
           
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }

       // GET User
       public function getSingleUser(){
        $sqlQuery = "SELECT
                    id, 
                    nama_depan, 
                    nama_belakang, 
                    email_id
                  FROM
                    ". $this->dbTable ."
                WHERE 
                   id = ?
                LIMIT 0,1";

        $stmt = $this->conn->prepare($sqlQuery);

        $stmt->bindParam(1, $this->id);

        $stmt->execute();

        $dataRow = $stmt->fetch(PDO::FETCH_ASSOC);
        
        $this->nama_depan = $dataRow['nama_depan'];
        $this->nama_belakang = $dataRow['nama_belakang'];
        $this->email_id = $dataRow['email_id'];
      
    }      
        

        // UPDATE User
        public function updateUser(){
            $sqlQuery = "UPDATE
                        ". $this->dbTable ."
                    SET
                    nama_depan = :nama_depan, 
                    nama_belakang = :nama_belakang, 
                    email_id = :email_id
                    WHERE 
                        id = :id";
        
            $stmt = $this->conn->prepare($sqlQuery);
        
            $this->nama_depan=htmlspecialchars(strip_tags($this->nama_depan));
            $this->nama_belakang=htmlspecialchars(strip_tags($this->nama_belakang));
            $this->email_id=htmlspecialchars(strip_tags($this->email_id));
            $this->id=htmlspecialchars(strip_tags($this->id));
        
            // bind data
            $stmt->bindParam(":nama_depan", $this->nama_depan);
            $stmt->bindParam(":nama_belakang", $this->nama_belakang);
            $stmt->bindParam(":email_id", $this->email_id);
            $stmt->bindParam(":id", $this->id);
        
            if($stmt->execute()){
               return true;
            }
            return false;
        }

        // DELETE User
        function deleteUser(){
            $sqlQuery = "DELETE FROM " . $this->dbTable . " WHERE id = ?";
            $stmt = $this->conn->prepare($sqlQuery);
        
            $this->id=htmlspecialchars(strip_tags($this->id));
        
            $stmt->bindParam(1, $this->id);
        
            if($stmt->execute()){
                return true;
            }
            return false;
        }

    }
?>